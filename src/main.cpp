#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Firebase_ESP_Client.h>
#include "addons/TokenHelper.h"
#include "addons/RTDBHelper.h"

#define SALIDA 5
#define ENTRADA 0
#define WIFI_SSID ""
#define WIFI_PASSWORD ""
#define DATABASE_URL "https://esp8266-test-a2db9-default-rtdb.firebaseio.com"
#define API_KEY "AIzaSyBSTn6IADNKN8D6A4jOhOpqG2pabJKZ6y8"

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

unsigned long sendDataPrevMillis = 0;
bool signupOK = false;
bool alarmaActivada = 1;
bool testeoAlarma = 0;
int microfono = 0; // 900=silencio, 500=saturacion
int sensibilidad = 1000; // umbral
int tiempoAlarma = 2000; // ms

void setup()
{
  pinMode(SALIDA,OUTPUT);
  pinMode(ENTRADA,INPUT);

  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Conectando a WiFi");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Conectado con IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  config.api_key = API_KEY;
  config.database_url = DATABASE_URL;

  if(Firebase.signUp(&config, &auth, "", ""))
  {
    Serial.println("signUp OK");
    signupOK = true;
  }
  else
  {
    Serial.printf("%s\n", config.signer.signupError.message.c_str());
  }
    config.token_status_callback = tokenStatusCallback;
    Firebase.begin(&config, &auth);
    Firebase.reconnectWiFi(true);
}

void loop()
{
  microfono = analogRead(ENTRADA);
  if(Firebase.ready() && signupOK && (millis() - sendDataPrevMillis > 1000 || sendDataPrevMillis == 0))
  {
    sendDataPrevMillis = millis(); 
    Firebase.RTDB.setInt (&fbdo, "sensor/microfono", microfono);
    if (Firebase.RTDB.getBool(&fbdo, "/opciones/alarmaActivada"))
    {
      if (fbdo.dataTypeEnum() == firebase_rtdb_data_type_boolean) 
      {
        alarmaActivada = fbdo.to<bool>();
        Serial.println(alarmaActivada);
      }
    }
    if (Firebase.RTDB.getBool(&fbdo, "/opciones/testeoAlarma"))
    {
      if (fbdo.dataTypeEnum() == firebase_rtdb_data_type_boolean) 
      {
        testeoAlarma = fbdo.to<bool>();
        Serial.println(testeoAlarma);
      }
    }
    if (Firebase.RTDB.getInt(&fbdo, "/opciones/sensibilidad"))
    {
      if (fbdo.dataTypeEnum() == firebase_rtdb_data_type_integer) 
      {
        sensibilidad = fbdo.to<int>();
        Serial.println(sensibilidad);
      }
    }
    if (Firebase.RTDB.getInt(&fbdo, "/opciones/tiempoAlarma"))
    {
      if (fbdo.dataTypeEnum() == firebase_rtdb_data_type_integer) 
      {
        tiempoAlarma= fbdo.to<int>();
        Serial.println(tiempoAlarma);
      }
    }
    else 
    {
      Serial.println(fbdo.errorReason());
    }
    if ((microfono<sensibilidad) && (alarmaActivada == 1) || (testeoAlarma == 1))
    {
      Firebase.RTDB.setBool (&fbdo, "sensor/estadoAlarma", true);
      digitalWrite(SALIDA,HIGH);
      delay(tiempoAlarma);
      digitalWrite(SALIDA,LOW);
    }
    else 
    {
      Firebase.RTDB.setBool (&fbdo, "sensor/estadoAlarma", false);
      digitalWrite(SALIDA,LOW);
    }
  }
}